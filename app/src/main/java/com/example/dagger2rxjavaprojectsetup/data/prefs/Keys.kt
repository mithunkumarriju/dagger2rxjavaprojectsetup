package com.example.dagger2rxjavaprojectsetup.data.prefs
enum class Keys {
    KEEP_ME_LOGGED_IN,
    DATA_BUNDLE,
    DEVICE_TOKEN,
    single_notification,
    un_seen_notification_count,
    seen_notification_list,
    IS_NOTIFICATION_ENABLED,
    APP_LANGUAGE,
}