package com.example.dagger2rxjavaprojectsetup.data.network

import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "No Internet Connection !"

}