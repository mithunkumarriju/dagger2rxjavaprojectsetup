package com.example.dagger2rxjavaprojectsetup.di.module

import Navigator
import android.content.Context
import com.example.dagger2rxjavaprojectsetup.base.BaseApplication
import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import com.example.dagger2rxjavaprojectsetup.rx.AppSchedulerProvider
import com.example.dagger2rxjavaprojectsetup.utils.AlertService
import com.example.dagger2rxjavaprojectsetup.utils.AppLogger
import com.example.dagger2rxjavaprojectsetup.utils.NetworkUtils
import com.example.dagger2rxjavaprojectsetup.utils.PermissionUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApplicationModule {

    @Provides
    fun provideContext(baseApp: BaseApplication): Context {
        return baseApp
    }

    @Provides
    @Singleton
    fun providePreferenceManager(context: Context): PreferenceManager {
        return PreferenceManager(context)
    }

    @Provides
    @Singleton
    fun providePermissionUtils(preferenceManager: PreferenceManager): PermissionUtils {
        return PermissionUtils(preferenceManager)
    }

    @Provides
    @Singleton
    fun provideAlertService(): AlertService {
        return AlertService()
    }

    @Provides
    @Singleton
    fun provideAppSchedule(): AppSchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideNavigator(): Navigator {
        return Navigator()
    }

    @Provides
    @Singleton
    fun provideAppLogger(): AppLogger {
        return AppLogger()
    }

    @Provides
    @Singleton
    fun provideNetworkUtils(): NetworkUtils {
        return NetworkUtils()
    }
}