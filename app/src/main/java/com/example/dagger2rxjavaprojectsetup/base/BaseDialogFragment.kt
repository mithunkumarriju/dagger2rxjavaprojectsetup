package com.example.dagger2rxjavaprojectsetup.base
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import butterknife.ButterKnife
import butterknife.Unbinder

abstract class BaseDialogFragment constructor(private var gravity: Int = Gravity.TOP) :
    DialogFragment() {

    private var viewUnbinder: Unbinder? = null
    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = getDialogView(inflater, container, savedInstanceState)
        viewUnbinder = ButterKnife.bind(this, rootView)
        return rootView
    }

    protected abstract fun getDialogView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.setGravity(gravity)
        isCancelable = false
        onDialogViewReady(arguments)
    }

    protected abstract fun onDialogViewReady(getArguments: Bundle?)

    override fun onDestroyView() {
        super.onDestroyView()
        if (viewUnbinder != null) {
            viewUnbinder?.unbind()
            viewUnbinder = null
        }
    }
}