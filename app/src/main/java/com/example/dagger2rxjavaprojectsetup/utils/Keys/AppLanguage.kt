package com.example.dagger2rxjavaprojectsetup.utils.Keys
enum class AppLanguage{
    ENGLISH,
    BENGALI
}