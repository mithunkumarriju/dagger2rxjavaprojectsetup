package com.example.dagger2rxjavaprojectsetup.base

import android.app.ProgressDialog
import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import butterknife.ButterKnife
import butterknife.Unbinder
import com.example.dagger2rxjavaprojectsetup.R


abstract class BaseActivity : AppCompatActivity() {

    protected val TAG: String  by lazy {
        this.javaClass.simpleName
    }

    private var progressDialog: ProgressDialog? = null
    private var unBinder: Unbinder? = null
    lateinit var mPrefs: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPrefs = PreferenceManager(getContext())
        setContentView(getContentView())
        unBinder = ButterKnife.bind(this)
        supportActionBar?.let {
            supportActionBar!!.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }

        onViewReady(savedInstanceState, intent)
    }

    abstract fun getContentView(): Int
    abstract fun onViewReady(savedInstanceState: Bundle?, intent: Intent)

    protected fun getContext(): Context {
        return this
    }

    fun showProgressDialog(message: String) {
        if (null == progressDialog) {
            progressDialog = ProgressDialog(this, R.style.MyAlertDialogStyle)
            progressDialog!!.setCancelable(false)
        }
        progressDialog!!.setMessage(message)
        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    fun hideProgressDialog() {
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item?.let {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return item?.let { super.onOptionsItemSelected(it) }
    }

    override fun onDestroy() {
//        if (BuildConfig.DEBUG) {
//            AppWatcher.objectWatcher.watch(this, TAG)
//        }
        super.onDestroy()
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
        }
        unBinder?.let {
            unBinder!!.unbind()
            unBinder = null
        }
    }


}