package com.example.dagger2rxjavaprojectsetup.di.component

import com.example.dagger2rxjavaprojectsetup.di.module.ApplicationModule
import com.example.dagger2rxjavaprojectsetup.base.BaseApplication
import com.example.dagger2rxjavaprojectsetup.di.module.ActivityBindingModule
import com.example.dagger2rxjavaprojectsetup.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    NetworkModule::class,
    ApplicationModule::class,
    ActivityBindingModule::class])

interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: BaseApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: BaseApplication)
}