package com.example.dagger2rxjavaprojectsetup.di.module

import com.example.dagger2rxjavaprojectsetup.data.network.ApiServiceBuilder
import com.example.dagger2rxjavaprojectsetup.utils.NetworkUtils
import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import com.example.dagger2rxjavaprojectsetup.data.network.RetrofitApiClient
import android.content.Context
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitApiClient(
        context: Context,
        networkUtils: NetworkUtils,
        preferenceManager: PreferenceManager
    ): Retrofit {
        return RetrofitApiClient.getRetrofit(context, networkUtils, preferenceManager)
    }

    @Provides
    @Singleton
    fun provideApiServiceBuilder(retrofit: Retrofit): ApiServiceBuilder {
        return ApiServiceBuilder(retrofit)
    }

    //////////////////////////////////////////////////////////////////
/*
    @Provides
    @Singleton
    fun provideUserApiService(apiServiceBuilder: com.example.dagger2rxjavaprojectsetup.data.network.ApiServiceBuilder): UserApiService {
        return apiServiceBuilder.buildService(UserApiService::class.java)
    }*/


}