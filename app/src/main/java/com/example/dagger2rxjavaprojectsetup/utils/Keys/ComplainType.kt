package com.example.dagger2rxjavaprojectsetup.utils.Keys
enum class ComplainType{
    ALL,
    SOLVED,
    UNSOLVED
}