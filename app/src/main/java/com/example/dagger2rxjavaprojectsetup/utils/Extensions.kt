package com.example.dagger2rxjavaprojectsetup.utils
import android.content.Context
import android.util.Base64
import android.widget.Toast
import java.nio.charset.StandardCharsets


fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this.applicationContext, message, duration).show()
}

fun String.encodeToBase64(): String {
    return Base64.encodeToString(this.toByteArray(), Base64.DEFAULT)
}

fun String.decodeFromBase64(): String {
    return String(Base64.decode(this.toByteArray(), Base64.DEFAULT), StandardCharsets.UTF_8)
}