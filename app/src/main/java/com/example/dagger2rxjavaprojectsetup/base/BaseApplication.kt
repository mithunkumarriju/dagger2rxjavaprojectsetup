package com.example.dagger2rxjavaprojectsetup.base

import android.app.Activity
import android.app.Application
import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject


class BaseApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var mActivityInjector: DispatchingAndroidInjector<Activity>

    var mAppLanguage: String? = null

    private val TAG = BaseApplication::class.java.simpleName

    @Inject
    lateinit var mPrefManager: PreferenceManager


    override fun onCreate() {
        super.onCreate()
        initDagger()

    }

    private fun initDagger() {
        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

     override fun activityInjector(): AndroidInjector<Activity> {
        return mActivityInjector
    }


}

