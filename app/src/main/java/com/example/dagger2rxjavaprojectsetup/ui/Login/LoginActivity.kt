package com.example.dagger2rxjavaprojectsetup.ui.Login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dagger2rxjavaprojectsetup.R

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}