package com.example.dagger2rxjavaprojectsetup.data.network
object APIs {
    const val BASE_URL = "http://scs.walletmix.biz"
    private const val SUB_URL = "/api"

    // APP VERSION
    const val GET_APP_VERSION = "$SUB_URL/app-version"

    // OTP
    const val GET_OTP = "$SUB_URL/send-otp"
    const val VERIFY_OTP = "$SUB_URL/verify-otp"

    // AUTH
    const val LOGIN = "$SUB_URL/login"
    const val REGISTRATION = "$SUB_URL/register"
    const val THANA_LIST = "$SUB_URL/thana-list"
    const val INIT_CHANGE_PIN = "$SUB_URL/init-change-pin"
    const val CONFIRM_CHANGE_PIN = "$SUB_URL/confirm-change-pin"
    const val CHANGE_REWARD_TYPE = "$SUB_URL/change-reward-type"
    const val LOGOUT = "$SUB_URL/logout"
    const val FORGET_PIN = "$BASE_URL$SUB_URL/forgot-pin"
    const val REFERRAL_HISTORY = "https://run.mocky.io/v3/547968f8-434b-43a3-a879-3ace1ad92dd3"

    // Add fund
    const val GET_PAYMENT_METHOD = "$SUB_URL/payment-methods"
    const val INIT_ADD_FUND = "$SUB_URL/init-add-fund"
    const val BKASH_PAYMENT = "$BASE_URL$SUB_URL/bkash-payment"
    const val NAGAD_PAYMENT = "$BASE_URL$SUB_URL/nagad-payment"

    const val STORE_DEVICE_TOKEN = "$SUB_URL/store-device-token"
    const val SSL_CONFIRMATION = "$SUB_URL/confirm-ssl-payment"

    // Utils
    const val FAQ = "$SUB_URL/faq-list"
    const val BANNER = "$SUB_URL/banner-list"
    const val CAMPAIGN_BANNER = "$SUB_URL/campaign-list"
    const val GET_ACCOUNTS = "$SUB_URL/get-accounts"
    const val SECONDARY_ACCOUNT_TOKEN = "$SUB_URL/get-secondary-account-token"
    const val REMOVE_LINKED_ACCOUNT = "$SUB_URL/remove-linked-account"
    const val ADD_ACCOUNT_INIT = "$SUB_URL/add-account-init"
    const val ADD_ACCOUNT_CONFIRM = "$SUB_URL/add-account-confirm"
    const val WALLET = "$SUB_URL/wallet"
    const val GET_OFFERS = "$SUB_URL/offers"

    const val MY_CIRCLE = "$BASE_URL$SUB_URL/my-circle"

    // PROFILE
    const val GET_PROFILE = "$SUB_URL/get-profile"
    const val UPDATE_PROFILE = "$SUB_URL/update-profile"



    // TRANSACTION
    const val TRANSACTION_SUMMERY = "$SUB_URL/transaction-summary"

    const val TRANSACTION_OUT_HISTORY = BASE_URL +
            "$SUB_URL/balance-out"
    const val TRANSACTION_IN_HISTORY = BASE_URL +
            "$SUB_URL/balance-in"
    const val AVAILABLE_POINT = "$SUB_URL/available-points"
    const val POINT_VERSION_SLOT = "$SUB_URL/point-conversion-slots"

    const val CONVERT_POINT = "$SUB_URL/convert-point"

    // Recharges
    const val CHECK_OFFER = "$SUB_URL/check-offer"
    const val INIT_RECHARGE = "$SUB_URL/init-recharge"
    const val CONFIRM_RECHARGE = "$SUB_URL/confirm-recharge"

    const val CONFIRM__VAS_RECHARGE = "$SUB_URL/confirm-vas-recharge"

    // NOTIFICATION
    const val NOTIFICATION = "$SUB_URL/user/get-user-notification"
    const val REMOVE_DEVICE_TOKEN = "$SUB_URL/user/remove-device-token"


    val NO_AUTH_URLs = arrayOf(
        "$BASE_URL$GET_APP_VERSION",
        "$BASE_URL$LOGIN",
        "$BASE_URL$REGISTRATION",
        "$BASE_URL$GET_OTP",
        "$BASE_URL$VERIFY_OTP",
        FORGET_PIN
    )

    val CACHING_URLs = arrayOf(
        "$BASE_URL$THANA_LIST",
        FORGET_PIN
    )
}