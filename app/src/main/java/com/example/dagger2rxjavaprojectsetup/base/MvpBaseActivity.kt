package com.example.dagger2rxjavaprojectsetup.base

import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.example.dagger2rxjavaprojectsetup.R
import com.example.dagger2rxjavaprojectsetup.utils.AlertService
import com.example.dagger2rxjavaprojectsetup.utils.AppLogger
import com.example.dagger2rxjavaprojectsetup.utils.NetworkUtils
import com.example.dagger2rxjavaprojectsetup.utils.showToast
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class MvpBaseActivity<P : BaseContract.Presenter> : AppCompatActivity(), BaseContract.View,
    HasSupportFragmentInjector {

    protected val TAG: String by lazy {
        this.javaClass.simpleName
    }

    private var progressDialog: ProgressDialog? = null
    private var unBinder: Unbinder? = null

    @Inject
    lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mPresenter: P

    @Inject
    lateinit var mAlertService: AlertService

    @Inject
    lateinit var mNetworkUtils: NetworkUtils

    @Inject
    lateinit var mAppLogger: AppLogger

    @Inject
    lateinit var mPrefManager: PreferenceManager

    var mAppLanguage: String? = null

    // Dialog >>>>
    var loader: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(getContentView())
        unBinder = ButterKnife.bind(this)
        supportActionBar?.let {
            supportActionBar!!.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }
        onViewReady(savedInstanceState, intent)
    }

    abstract fun getContentView(): Int
    abstract fun onViewReady(savedInstanceState: Bundle?, intent: Intent)

    protected fun showProgressDialog(message: String) {
        val loaderBuilder = AlertDialog.Builder(this)
        loaderBuilder.setCancelable(false)
        val loadingView =
            LayoutInflater.from(this).inflate(R.layout.loading_view, null)
        val gifImgView =
            loadingView.findViewById<ImageView>(R.id.img_loader)
        Glide.with(this).load(R.raw.loader).into(gifImgView)
        loaderBuilder.setView(loadingView)
        loader = loaderBuilder.create()
        loader?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        loader?.show()
    }
/*
    protected fun showSuccessDialog(title: String? = null, message: String) {
        val dialogSuccess: DialogSuccess = DialogSuccess.newInstance(title, message)
        dialogSuccess.show(supportFragmentManager, DialogSuccess.TAG)
    }

    protected fun showErrorDialog(title: String? = null, message: String) {
        val dialogError: DialogError = DialogError.newInstance(title, message)
        dialogError.show(supportFragmentManager, DialogError.TAG)
    }*/

    protected fun hideProgressDialog() {
        loader?.cancel()
    }

    protected fun showAlert(title: String? = null, message: String) {
        mAlertService.showAlert(getContext(), title, message)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item?.let {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mPresenter.detachView()
        mAlertService.onDestroy()
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = null
        }
        unBinder?.let {
            unBinder!!.unbind()
            unBinder = null
        }
        mPresenter.clearDisposable()

//        if (BuildConfig.DEBUG) {
//            AppWatcher.objectWatcher.watch(this, TAG)
//        }
        super.onDestroy()
    }

    override fun getContext(): Context {
        return this
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }

    override fun onNetworkCallEnded() {
        hideProgressDialog()
    }

    override fun onNetworkUnavailable() {
        mAlertService.showAlert(
            getContext(),
            getString(R.string.no_internet_connection),
            getString(R.string.no_internet_msg)
        )
    }

    override fun onServerError() {
        mAlertService.showToast(getContext(), getString(R.string.something_wrong))
    }

    override fun onTimeOutError() {
        mAlertService.showToast(getContext(), getString(R.string.something_wrong))
    }

    override fun onUserDidTooManyAttempts(errorMsg: String) {
        // com.example.dagger2rxjavaprojectsetup.utils.showToast(errorMsg)
    }

    override fun onUserUnauthorized(errorMessage: String) {
        showToast(errorMessage)
//        mPrefManager.clearPreference()
//        Navigator.sharedInstance.back(getContext(), LoginActivity::class.java)
//        finish()
    }

    override fun onUserDisabled(errorMsg: String) {
        showAlert(message = errorMsg)
    }

    override fun onSystemUpgrading() {
        mAlertService.showConfirmationAlert(
            getContext(),
            getString(R.string.maintenance_title),
            getString(R.string.maintenance_message),
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {

                }

                override fun positiveBtnDidTapped() {
                    System.exit(0)
                }
            })
    }

  /*  override fun onExpectationFailed(message: String) {
        showErrorDialog(message = message)
    }

    override fun onValidationFailed(messages: LinkedHashMap<String, String>) {
        showErrorDialog(message = messages.values.first())
    }*/

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mFragmentInjector
    }



}