package com.example.dagger2rxjavaprojectsetup.data.prefs

object PrefKeys {

    const val TAPPED_NOTIFICATION = "tapped_notification"
    const val DEVICE_TOKEN: String = "device_token"
    const val REWARD_TYPE: String = "reward_type"
    const val CHILD_TOKEN = "child_token"
    const val TOKEN = "token"
    const val HAS_CHILD = "has_child"
    const val IS_CHILD_SELECCTED = "is_selected_child"
    const val PREV_LOGIN_MOBILE_NUMBER = "prev_login_mobile_number"

    // Auth
    const val IS_VERIFIED = "is_verified"
    const val IS_REGISTERED = "is_registered"
    const val APP_SIGNING_KEY = "app_signing_key"
}