package com.example.dagger2rxjavaprojectsetup.utils.Keys
object IntentKeys {
    const val MY_COMMISSION = "my_commission"
    const val TAPPED_NOTIFICATION = "tapped_notification"
    const val CURRENT_PIN: String = "current_pin"
    const val NEW_PIN: String = "new_t_pin"
    const val CONFIRM_NEW_PIN: String = "confirm_new_pin"
    const val VAS_TYPE: String = "vas_type"
    const val OFFER_TYPE: String = "offer_type"
    const val COMPLAIN_TYPE: String = "complain_type"
    const val LOCATION_ACTION = "current_location"
    const val LONGITUDE = "longitude"
    const val LATITUDE = "latitude"

    const val DATA_BUNDLE = "data_bundle"
    const val MOBILE_NUMBER = "mobile_number"
    const val REMAINING_TIME_IN_SECONDS_FOR_OTP = "remaining_time_for_otp"
    const val SELECTED_LOCATION_ID = "selected_location_id"
    const val SELECTED_LOCATION_NAME = "selected_location_name"


    const val REF_ID = "ref_id"
    const val AMOUNT = "amount"
    const val REMARKS = "remarks"
    const val PAYMENT_METHODS = "remarks"
    const val PAYMENT_METHOD_NAME = "payment_method_name"
    const val PAYMENT_ID = "payment_id"
    const val TRANSACTION_ID = "transaction_id"
    const val PAYMENT_URL = "payment_url"
    const val VERIFICATION_TYPE = "verification_type"
    const val MFS_STATUS = "mfs_status"
    const val AVAILABLE_POINT = "available_point"
    const val LAST_UPDATED = "last_update"
}
