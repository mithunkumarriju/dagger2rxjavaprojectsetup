package com.example.dagger2rxjavaprojectsetup.base

import Navigator
import com.example.dagger2rxjavaprojectsetup.data.prefs.PreferenceManager
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.example.dagger2rxjavaprojectsetup.ui.Login.LoginActivity
import com.example.dagger2rxjavaprojectsetup.R
import com.example.dagger2rxjavaprojectsetup.utils.AlertService
import com.example.dagger2rxjavaprojectsetup.utils.AppLogger
import com.example.dagger2rxjavaprojectsetup.utils.NetworkUtils
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment<P : BaseContract.Presenter> : Fragment(), BaseContract.View {

    protected val TAG: String by lazy {
        this.javaClass.simpleName
    }

    private var progressDialog: ProgressDialog? = null
    private var viewUnbinder: Unbinder? = null

    @Inject
    lateinit var mPresenter: P

    @Inject
    lateinit var mAlertService: AlertService

    @Inject
    lateinit var mNetworkUtils: NetworkUtils

    @Inject
    lateinit var mAppLogger: AppLogger

    @Inject
    lateinit var mPrefManager: PreferenceManager

    lateinit var rootView: View

    var mAppLanguage: String? = null

    // Dialog >>>>
    var loader: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @Nullable
    override fun onCreateView(
        @NonNull inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        rootView = getFragmentView(inflater, container, savedInstanceState)
        viewUnbinder = ButterKnife.bind(this, rootView)
        onViewReady(arguments)
        return rootView
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    protected abstract fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View

    protected abstract fun onViewReady(getArguments: Bundle?)

/*
    fun showSuccessDialog(title: String? = null, message: String) {
        val dialogSuccess: DialogSuccess = DialogSuccess.newInstance(title, message)
        fragmentManager?.let { fm ->
            dialogSuccess.show(fm, DialogSuccess.TAG)
        }
    }

    fun showErrorDialog(title: String? = null, message: String) {
        val dialogError: DialogError = DialogError.newInstance(title, message)
        fragmentManager?.let { fm ->
            dialogError.show(fm, DialogError.TAG)
        }
    }*/

    private fun showProgressDialog(message: String) {
        if (loader == null) {
            val loaderBuilder = AlertDialog.Builder(context)
            loaderBuilder.setCancelable(false)
            val loadingView =
                LayoutInflater.from(context).inflate(R.layout.loading_view, null)
            val gifImgView =
                loadingView.findViewById<ImageView>(R.id.img_loader)
            Glide.with(context).load(R.raw.loader).into(gifImgView)
            loaderBuilder.setView(loadingView)
            loader = loaderBuilder.create()
            loader?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        loader?.show()
    }

    private fun hideProgressDialog() {
        loader?.cancel()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
//        if (BuildConfig.DEBUG) {
//            AppWatcher.objectWatcher.watch(this, TAG)
//        }
        mPresenter.clearDisposable()
        mPresenter.detachView()
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
        }
        viewUnbinder?.let {
            viewUnbinder!!.unbind()
            viewUnbinder = null
        }
        super.onDestroy()
    }


    override fun getContext(): Context {
       return requireActivity()
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        if (activity != null)
            showProgressDialog(loadingMessage)
    }

    override fun onNetworkCallEnded() {
        if (activity != null)
            hideProgressDialog()
    }

    override fun onNetworkUnavailable() {
        if (activity != null)
            mAlertService.showAlert(
                context,
                getString(R.string.no_internet_connection),
                getString(R.string.no_internet_msg)
            )
    }

    override fun onServerError() {
        if (activity != null)
            mAlertService.showToast(context, getString(R.string.something_wrong))
    }

    override fun onTimeOutError() {
        if (activity != null)
            mAlertService.showToast(context, getString(R.string.something_wrong))
    }

    override fun onUserDidTooManyAttempts(errorMsg: String) {
        mAlertService.showToast(context, errorMsg)
    }

    override fun onUserUnauthorized(errorMessage: String) {
        if (activity != null) {
            mPrefManager.clearPreference()
            Navigator.sharedInstance.back(context, LoginActivity::class.java)
        }
    }

    override fun onUserDisabled(errorMsg: String) {
        mAlertService.showConfirmationAlert(
            context,
            null,
            errorMsg,
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {

                }

                override fun positiveBtnDidTapped() {
                    System.exit(0)
                }
            })
    }

   /* override fun onExpectationFailed(message: String) {
        showErrorDialog(message = message)
    }

    override fun onValidationFailed(messages: LinkedHashMap<String, String>) {
        showErrorDialog(message = messages.values.first())
    }*/

    fun showAlert(title: String? = null, message: String) {
        mAlertService.showAlert(getContext(), title, message)
    }


    override fun onSystemUpgrading() {
        if (activity != null)
            mAlertService.showConfirmationAlert(
                getContext(),
                getString(R.string.maintenance_title),
                getString(R.string.maintenance_message),
                null,
                getString(R.string.okay),
                object : AlertService.AlertListener {
                    override fun negativeBtnDidTapped() {

                    }

                    override fun positiveBtnDidTapped() {
                        System.exit(0)
                    }
                })
    }


}