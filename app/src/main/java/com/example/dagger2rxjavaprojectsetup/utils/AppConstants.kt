package com.example.dagger2rxjavaprojectsetup.utils
object AppConstants {
    const val LAUNCH_CAMERA = 101
    const val LAUNCH_GALLERY = 102
    const val LAUNCH_SELECT_REGION_ACTIVITY: Int = 103
    const val LAUNCH_MFS_ACTIVITY: Int = 103

    const val REQUEST_CAMERA = 201
    const val REQUEST_GALLERY = 202
    const val REQUEST_CONTACT = 123
}

object OtpVerificationType {
    const val ADD_ACCOUNT = 1
    const val CHANGE_PIN = 2
    const val REGULAR = 0
}


object SOURCE_TYPE {
    const val ADD_FUND = 1
    const val ADD_FUND_COMMISSION = 2
    const val POINT_CONVERSION = 3
    const val CAMPAIGN = 4
}


object PaymentMethod {
    const val bKash = 1
    const val Nagad = 2
    const val SSL = 3
}

object OperatorType {
    const val PREPAID = "0"
    const val POSTPAID = "1"
}


object OfferType {
    const val BUNDLE = "0"
    const val INTERNET = "1"
    const val VOICE = "2"
    const val VAS = "3"
}

object VasType {
    const val SOCIAL_AND_CHAT = "0"
    const val MOBILE_ASSISTANT = "1"
    const val MUSIC_AND_ENTERTAINMENT = "2"
    const val NEWS_AND_INFO = "3"
    const val LIFE_STYLE = "4"
}

object MfsStatus {
    const val SUCCESS = "success"
    const val FAILED = "failed"
    const val CANCELLED = "cancelled"
}


object RechargeType {
    const val BUNDLE = 0
    const val INTERNET = 1
    const val VOICE = 2
    const val VAS = 3
    const val EASY_LOAD = 4
}

object RewardType {
    const val COMMISSION = 0
    const val POINT = 1
}

object HistoryType {
    const val CURRENT_MONTH = "1"
    const val LAST_30_DAYS = "2"
}

object BannerType {
    const val REFERRAL = "1"
    const val CAMPAIGN = "2"
    const val OTHERS = "3"
}