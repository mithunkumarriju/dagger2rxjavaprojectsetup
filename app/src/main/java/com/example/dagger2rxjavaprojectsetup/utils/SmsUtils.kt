package com.example.dagger2rxjavaprojectsetup.utils

import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.telephony.SmsManager
import com.example.dagger2rxjavaprojectsetup.R
import com.example.dagger2rxjavaprojectsetup.utils.AlertService
import com.example.dagger2rxjavaprojectsetup.utils.showToast

class SmsUtils(val mContext: Context) {

    private val SENT = "${mContext.applicationContext.packageName}SMS_SENT"
    private val DELIVERED = "${mContext.applicationContext.packageName}SMS_DELIVERED"
    private val alertService = AlertService()

    fun sendSMS(
        mContext: Context,
        phoneNo: String,
        messageInfo: String
    ) {
        try {
            // Creating Pending Intent
            val sentPI = PendingIntent.getBroadcast(mContext, 0, Intent(SENT), 0)
            val deliveredPI = PendingIntent.getBroadcast(mContext, 0, Intent(DELIVERED), 0)


            val smsManager: SmsManager = SmsManager.getDefault()
            val arrSMS = smsManager.divideMessage(messageInfo)
            smsManager.sendMultipartTextMessage(
                phoneNo,
                null,
                arrSMS,
                arrayListOf(sentPI),
                arrayListOf(deliveredPI)
            )

        } catch (ex: Exception) {
            //
        }
    }


    // Call this method in onStart of the Activity..
    fun registerSentDeliveredReceiver() {
        mContext.registerReceiver(sentBroadCastReceiver, IntentFilter(SENT))
        mContext.registerReceiver(deliveredBroadCastReceiver, IntentFilter(DELIVERED))

    }

    // Call this method in onStop of the activity...
    fun unRegisterSentDeliveredReceiver() {
        mContext.unregisterReceiver(sentBroadCastReceiver)
        mContext.unregisterReceiver(deliveredBroadCastReceiver)
    }


    private val sentBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    alertService.showAlert(mContext, null, mContext.getString(R.string.emergency_msg_sent))
                }
                SmsManager.RESULT_ERROR_GENERIC_FAILURE -> mContext.showToast("Please set a default SIM of your handset.")
                SmsManager.RESULT_ERROR_RADIO_OFF -> mContext.showToast("FAILED")
                SmsManager.RESULT_ERROR_NULL_PDU -> mContext.showToast("FAILED")
                SmsManager.RESULT_ERROR_NO_SERVICE -> mContext.showToast("NO SERVICE")

            }
        }
    }

    private val deliveredBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (resultCode) {
                // Activity.RESULT_OK -> mContext.com.example.dagger2rxjavaprojectsetup.utils.showToast("DELIVERED")
                // Activity.RESULT_CANCELED -> mContext.com.example.dagger2rxjavaprojectsetup.utils.showToast("FAILED TO DELIVER !")
            }
        }
    }
}
/**
 *  SendSms is only for English message
 *  SendMultiPart Sms is using here because it can send both english and bengali sms...
 * */