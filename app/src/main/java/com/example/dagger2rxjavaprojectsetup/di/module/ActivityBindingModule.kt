package com.example.dagger2rxjavaprojectsetup.di.module

import MainActivity
import com.example.dagger2rxjavaprojectsetup.ui.Home.MainActivityViewModule
import com.example.dagger2rxjavaprojectsetup.di.scope.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [FragmentBindingModule::class])
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityViewModule::class])
    abstract fun bindHomeActivity(): MainActivity

}